# EpicSlider #

*Note: This is currently in build, and is a while from production being released. Feel free to notify me of any bugs/issues and I'll add them to my TODO list.*

### Demo ###

* [Link to the project on my site](http://labs.enijar.net/epic-slider)

### How do I get set up? ###

* Download [jQuery](http://code.jquery.com/ui/1.10.4/jquery-ui.min.js)
* Download [jQuery UI](https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js)
* Download [Epic Slider JS](http://labs.enijar.net/epic-slider/js/epicSlider.js)
* Download [Epic Slider CSS](http://labs.enijar.net/epic-slider/css/main.css)

The following HTML specifies how to setup the markup for Epic Slider. Each scene should have a **data-scene** attribute assigned to it. This number will determine what position it appears in the slider. There must be a scene number of 1 and it must only increment by 1 for the slider to work. In this example we have 3 scenes to we set our first slider's **data-scene** to 1 and our last to 3.

### HTML ###

```
#!HTML

<div id="epic-slider-container">
    <div id="epic-slider-scenes">
        <div class="epic-slider-scene-container" data-scene="1">
            <img src="http://lorempixel.com/960/350/sports/1/">
        </div>

        <div class="epic-slider-scene-container" data-scene="2">
            <img src="http://lorempixel.com/960/350/sports/2/">
        </div>

        <div class="epic-slider-scene-container" data-scene="3">
            <img src="http://lorempixel.com/960/350/sports/3/">
        </div>
    </div>
</div>
```


### Contribution guidelines ###

* Writing tests
* Code review
* Bug/issue reports

### Who do I talk to? ###

* Repo owner or admin